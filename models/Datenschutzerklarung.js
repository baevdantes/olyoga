var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Datenschutzerklarung Model
 * ==========
 */

var Datenschutzerklarung   = new keystone.List('Datenschutzerklarung', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

Datenschutzerklarung.add({
	title: { type: String, required: true },
});

Datenschutzerklarung.register();
